from trainerbase.gui import (
    CodeInjectionUI,
    GameObjectUI,
    SeparatorUI,
    SpeedHackUI,
    add_components,
    simple_trainerbase_menu,
)

from injections import one_hit_kill
from objects import corruption, gold, hp, mana, max_hp, max_mana, max_minions


@simple_trainerbase_menu("Overlord", 700, 370)
def run_menu():
    add_components(
        GameObjectUI(gold, "Gold", default_setter_input_value=1_000_000),
        GameObjectUI(max_minions, "Max Minions"),
        GameObjectUI(corruption, "Corruption"),
        SeparatorUI(),
        GameObjectUI(hp, "HP", "PageUp"),
        GameObjectUI(max_hp, "Max HP"),
        SeparatorUI(),
        GameObjectUI(mana, "Mana", "PageDown"),
        GameObjectUI(max_mana, "Max Mana"),
        SeparatorUI(),
        CodeInjectionUI(one_hit_kill, "One Hit Kill", "Delete"),
        SeparatorUI(),
        SpeedHackUI(key="Alt"),
    )
