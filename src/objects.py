from trainerbase.gameobject import GameFloat, GameUnsignedInt
from trainerbase.memory import Address

from memory import player_base_pointer


player_base_address = Address(player_base_pointer)
avatar = player_base_address + [0x78]
life_tracker = avatar + [0x424]

gold = GameUnsignedInt(player_base_address + [0x1F8])
max_minions = GameUnsignedInt(player_base_address + [0x204])
corruption = GameUnsignedInt(player_base_address + [0x200])

max_mana = GameFloat(player_base_address + [0x244])
mana = GameFloat(avatar + [0x470])

hp = GameFloat(life_tracker + [0xD4])
max_hp = GameFloat(life_tracker + [0xD8])
