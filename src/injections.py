from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import player_base_pointer


update_player_base_pointer = AllocatingCodeInjection(
    pm.base_address + 0x2015BA,
    f"""
        mov edx, [esi + 0x1F8]
        mov [{player_base_pointer}], esi
    """,
    original_code_length=6,
)

one_hit_kill = AllocatingCodeInjection(
    pm.base_address + 0x2CC6F1,
    f"""
        push eax

        mov eax, [{player_base_pointer}]

        cmp eax, 0
        je skip

        cmp dword [ebp + 0x130], eax
        je skip

        mov dword [ebp + 0xE4], 1.0

        skip:

        pop eax

        fcomp dword [ebp + 0xE4]
    """,
    original_code_length=6,
)
